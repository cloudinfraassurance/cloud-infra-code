#!/bin/bash
sudo su
yum update -y
# shellcheck disable=SC2154
echo "${ssh_key}" > /home/ec2-user/273162Mac-Demo.pem
chmod 400 /home/ec2-user/273162Mac-Demo.pem
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubectl;
mkdir ~/.aws
# shellcheck disable=SC2154
echo -e "[default]\nregion=${aws_region}\n" >> ~/.aws/config;
# shellcheck disable=SC2154
echo -e "[default]\naws_access_key_id=${aws_key}\naws_secret_access_key=${aws_secret}" >> ~/.aws/credentials;
# shellcheck disable=SC2154
aws eks --region "${aws_region}" update-kubeconfig --name "${kube_cluster}"
sed -i 's/%%CLUSTER_NAME%%/'"${kube_cluster}"'/g' /home/ec2-user/recycle_jump_box.sh;
# shellcheck source=/root/.bashrc
export KUBECONFIG=$KUBECONFIG:/root/.kube/config
kubectl config view
while [[ "$(kubectl config view | grep -oP '(?<=- CIA_).*?(?=-local)')" != "Cluster" ]];
do
  sleep 20;
  echo "*********************Waiting for kubernetes cluster to be up and running*********************";
done
kubectl create -f \
https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl create -f /home/ec2-user/kubernetes-dashboard.yaml
kubectl create -f \
https://raw.githubusercontent.com/hashicorp/learn-terraform-provision-eks-cluster/master/kubernetes-dashboard-admin.rbac.yaml
kubectl create -f /home/ec2-user/cluster-autoscaler-autodiscover.yaml
kubectl create -f /home/ec2-user/mysql-configmap.yaml
kubectl create -f /home/ec2-user/mysql-services.yaml
kubectl create -f /home/ec2-user/mysql-statefulset.yaml
#
kubectl get pods --all-namespaces -o wide
kubectl get svc --all-namespaces -o wide
kubectl get StatefulSet --all-namespaces -o wide
kubectl get deployment --all-namespaces -o wide
kubectl get PersistentVolume --all-namespaces -o wide
kubectl get PersistentVolumeClaim --all-namespaces -o wide
kubectl get ConfigMap --all-namespaces -o wide
kubectl get node --all-namespaces -o wide
#
kubectl -n kube-system describe secret "$(kubectl -n kube-system get secret | \
grep service-controller-token | awk '{print $1}')"
kubectl cluster-info;

while [[ "$(kubectl get pods -l statefulset.kubernetes.io/pod-name=mysql-0 --field-selector=status.phase=Running| \
grep -c Running)" != "1" ]];
do
  sleep 20;
	echo "*********************Waiting for db service to be up and running*********************";
done
ssh -i "/home/ec2-user/273162Mac-Demo.pem" -o StrictHostKeyChecking=no -tt "ec2-user@${web_ip}" "sudo su<<- EOF
sed -i 's/DB_NAME_VALUE/'$(kubectl get svc mysql -o jsonpath='{.status.loadBalancer.ingress[*].hostname}')'/g' /var/www/html/index.php;
EOF";

#kubectl proxy --port=8080 --address=0.0.0.0 --disable-filter=true &
#kubectl port-forward svc/kubernetes-dashboard -n kube-system 6443:443 &
#ssh -i 273162Mac-Demo.pem ec2-user@3.9.175.169 -L 6443:127.0.0.1:6443
