kubectl delete -f /home/ec2-user/mysql-statefulset.yaml
kubectl delete -f /home/ec2-user/mysql-services.yaml
kubectl delete -f /home/ec2-user/mysql-configmap.yaml
kubectl delete -f /home/ec2-user/cluster-autoscaler-autodiscover.yaml
kubectl delete -f https://raw.githubusercontent.com/hashicorp/learn-terraform-provision-eks-cluster/master/kubernetes-dashboard-admin.rbac.yaml
kubectl delete -f /home/ec2-user/kubernetes-dashboard.yaml
kubectl delete -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

IFS=' '
read -r -a ary <<<"$(
  kubectl get PersistentVolumeClaim -o=jsonpath='{$.items[*].metadata.name}'
  echo
)"
for key in "${!ary[@]}"; do
  kubectl delete pvc "${ary[$key]}"
done