variable "keypair" {
  default = "273162Mac-Demo"
}
variable "instance_type" {
//  default = "t2.medium"
  default = "t2.small"
}
variable "domain_name" {
  default = "cia.internal"
}
variable "aws_region" {
  type        = string
  default     = "eu-west-2"
}
variable "ami_linux" {
  type = map(string)
  default = {
    eu-west-2 = "ami-0e80a462ede03e653"
    us-east-1 = "ami-14c5486b"
  }
}
variable "instance_tag_name" {
  type        = string
  default     = "CIA"
}
variable "cluster_name" {
  type        = string
  default     = "_Cluster-local"
}
variable "all_range_cidr" {
  type        = list(string)
  default     = ["0.0.0.0/0"]
}
variable "main_vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
}
variable "public_subnet_cidr" {
  type        = list(string)
  default     = ["10.0.0.0/24", "10.0.1.0/24"]
}
variable "private_subnet_cidr" {
  type        = list(string)
  default     = ["10.0.2.0/24", "10.0.3.0/24"]
}