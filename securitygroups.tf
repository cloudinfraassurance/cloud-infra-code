module "jump_box_sg" {
  source = "terraform-aws-modules/security-group/aws"
  version = "3.18.0"

  name        = "${var.instance_tag_name}_SG_JBox"
  description = "Security group for user-service with custom ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "out ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]
}
module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws"
  version = "3.18.0"

  name        = "${var.instance_tag_name}_SG_Web"
  description = "Security group for user-service with custom ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = var.all_range_cidr[0]
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "out ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]
}
module "db_server_sg" {
  source = "terraform-aws-modules/security-group/aws"
  version = "3.18.0"

  name        = "${var.instance_tag_name}_SG_DB"
  description = "Security group for user-service with custom ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "out ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]
}