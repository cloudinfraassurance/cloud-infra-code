resource "aws_instance" "jump_box" {
  associate_public_ip_address = true
  ami                         = lookup(var.ami_linux, var.aws_region)
  instance_type               = var.instance_type
  key_name                    = var.keypair
  monitoring                  = true
  vpc_security_group_ids      = [module.jump_box_sg.this_security_group_id]
  subnet_id                   = module.vpc.public_subnets[0]
  user_data                   = data.template_file.jump_box.rendered
  tags = {
    Name  = "${var.instance_tag_name} Jump Box"
    TagGroup  = "CIAGroup"
  }
  depends_on = [module.eks, module.vpc, module.jump_box_sg]
}

resource "null_resource" "ec2_jump_box_null_resource" {
  triggers = {
    ssh_key = data.aws_ssm_parameter.SSHKey.value
    instance_public_ip = aws_instance.jump_box.public_ip
  }
  connection {
    host        = self.triggers.instance_public_ip
    user        = "ec2-user"
    private_key = self.triggers.ssh_key
  }
  provisioner "file" {
    source        = "kb8/"
    destination   = "/home/ec2-user/"
  }
  provisioner "remote-exec" {
    when    = destroy
    inline = [
      "sudo bash -c 'chmod +x /home/ec2-user/recycle_jump_box.sh'",
      "sudo bash -c 'sed -i -e 's/\r$//' /home/ec2-user/recycle_jump_box.sh'",
      "sudo bash -c 'sh /home/ec2-user/recycle_jump_box.sh'",
    ]
  }
  depends_on = [aws_instance.jump_box]
}

module "ec2_web" {
  source                      = "terraform-aws-modules/ec2-instance/aws"
  version                     = "2.17.0"
  name                        = "${var.instance_tag_name} Web Server"
  instance_count              = 1
  associate_public_ip_address = true
  ami                         = lookup(var.ami_linux, var.aws_region)
  instance_type               = var.instance_type
  key_name                    = var.keypair
  monitoring                  = true
  vpc_security_group_ids      = [module.web_server_sg.this_security_group_id]
  subnet_id                   = module.vpc.public_subnets[0]
  user_data                   = file("shell/install_web.sh")
  tags = {
    TagGroup  = "CIAGroup"
  }
  depends_on = [module.web_server_sg]
}