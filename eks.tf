module "eks" {
  source                      = "terraform-aws-modules/eks/aws"
  version                     = "15.1.0"
  cluster_name                = "${var.instance_tag_name}${var.cluster_name}"
  cluster_version             = "1.19"
  vpc_id                      = module.vpc.vpc_id
  worker_security_group_id    = module.db_server_sg.this_security_group_id
  subnets                     = module.vpc.private_subnets
  worker_groups = [
    {
      name                      = "workers"
      instance_type             = var.instance_type
      root_volume_type          = "gp2"
      # Auto scaling group
      asg_min_size              = 3
      asg_max_size              = 9
      asg_desired_capacity      = 4
      health_check_type         = "EC2"
      health_check_grace_period = 1
      tags = [
        {
          key                 = "kubernetes.io/cluster/${var.instance_tag_name}${var.cluster_name}"
          value               = "owned"
          propagate_at_launch = true
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${var.instance_tag_name}${var.cluster_name}"
          value               = "owned"
          propagate_at_launch = true
        },
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          value               = true
          propagate_at_launch = true
        },
      ]
    }
  ]
    tags = {
    environment   = "development"
    TagGroup      = "CIAGroup"
    }
  depends_on = [module.vpc, module.db_server_sg]
}