output "main_vpc_id" {
  value = module.vpc.vpc_id
}
output "public_subnet_ids" {
  value = module.vpc.public_subnets
}
output "private_subnet_ids" {
  value = module.vpc.private_subnets
}
output "stack_region" {
  value = data.aws_region.stack_region.id
}
output "instance_id_app" {
  value = module.ec2_web.id
}
output "instance_public_ip_app" {
  value = module.ec2_web.public_ip[0]
}
output "instance_public_ip_jump_box" {
  value = aws_instance.jump_box.public_ip
}
output "ssh_key" {
  value = data.aws_ssm_parameter.SSHKey.value
  sensitive = true
}
