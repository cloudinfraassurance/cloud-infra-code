resource "aws_autoscaling_policy" "agents_scale_up" {
  name                    = "${var.instance_tag_name}${var.cluster_name}_agents_scale_up"
  scaling_adjustment      = 1
  adjustment_type         = "ChangeInCapacity"
  autoscaling_group_name  = module.eks.workers_asg_names[0]
  policy_type             = "SimpleScaling"
  cooldown                = 1
}
resource "aws_cloudwatch_metric_alarm" "agents_scale_up_metric_alarm" {
  alarm_name          = "${var.instance_tag_name}${var.cluster_name}_agents_scale_up_metric_alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "10"
  dimensions = {
    AutoScalingGroupName = module.eks.workers_asg_names[0]
  }
  actions_enabled     = true
  alarm_description   = "This metric monitors ec2 cpu utilization"
  alarm_actions       = [aws_autoscaling_policy.agents_scale_up.arn]
}
resource "aws_autoscaling_policy" "agents_scale_down" {
  name                    = "${var.instance_tag_name}${var.cluster_name}_agents_scale_down"
  scaling_adjustment      = -1
  adjustment_type         = "ChangeInCapacity"
  autoscaling_group_name  = module.eks.workers_asg_names[0]
  policy_type             = "SimpleScaling"
  cooldown                = 1
}
resource "aws_cloudwatch_metric_alarm" "agents_scale_down_metric_alarm" {
  alarm_name          = "${var.instance_tag_name}${var.cluster_name}_agents_scale_down_metric_alarm"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "5"
  dimensions = {
    AutoScalingGroupName = module.eks.workers_asg_names[0]
  }
  actions_enabled = true
  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.agents_scale_down.arn]
}
resource "aws_iam_role_policy_attachment" "workers_autoscaling" {
  policy_arn = aws_iam_policy.worker_autoscaling.arn
  role       = module.eks.worker_iam_role_name
}
resource "aws_iam_policy" "worker_autoscaling" {
  name_prefix = "${var.instance_tag_name}${var.cluster_name}_worker_autoscaling"
  description = "EKS worker node autoscaling policy for cluster ${var.instance_tag_name}${var.cluster_name}"
  policy      = data.aws_iam_policy_document.worker_autoscaling.json
  path        = "/"
}
data "aws_iam_policy_document" "worker_autoscaling" {
  statement {
    sid    = "eksWorkerAutoscalingAll"
    effect = "Allow"
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeTags",
      "ec2:DescribeLaunchTemplateVersions",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "eksWorkerAutoscalingOwn"
    effect = "Allow"
    actions = [
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "autoscaling:UpdateAutoScalingGroup",
    ]
    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/kubernetes.io/cluster/${var.instance_tag_name}${var.cluster_name}"
      values   = ["owned"]
    }

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled"
      values   = ["true"]
    }
  }
}