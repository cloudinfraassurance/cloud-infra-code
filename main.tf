terraform {
  required_version  = ">= 0.13.5"
}

data "aws_region" "stack_region" {
  provider          = aws.cloud_provider
}

data "aws_ssm_parameter" "SSHKey" {
  name              = "SSHKey"
}

data "aws_ssm_parameter" "CloudKey" {
  name              = "CloudKey"
}

data "aws_ssm_parameter" "CloudSecret" {
  name              = "CloudSecret"
}
data "template_file" "jump_box" {
  template          = file("shell/install_jump_box.sh")
  vars              = {
    ssh_key         = data.aws_ssm_parameter.SSHKey.value
    aws_region      = data.aws_region.stack_region.id
    kube_cluster    = "${var.instance_tag_name}${var.cluster_name}"
    aws_key         = data.aws_ssm_parameter.CloudKey.value
    aws_secret      = data.aws_ssm_parameter.CloudSecret.value
    web_ip          = module.ec2_web.public_ip[0]
  }
}
resource "random_id" "server" {
  byte_length       = 6
}
data "aws_eks_cluster" "cluster" {
  name              = module.eks.cluster_id
}
data "aws_eks_cluster_auth" "cluster" {
  name              = module.eks.cluster_id
}
provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}