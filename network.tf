provider "aws" {
  region = var.aws_region
}
provider "aws" {
  alias  = "cloud_provider"
  region = var.aws_region
}
data "aws_availability_zones" "available" {}
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.77.0"

  name                                = "${var.instance_tag_name}_VPC"
  cidr                                = var.main_vpc_cidr
  azs                                 = data.aws_availability_zones.available.names
  private_subnets                     = var.private_subnet_cidr
  public_subnets                      = var.public_subnet_cidr
  enable_nat_gateway                  = true
  single_nat_gateway                  = true
  create_igw                          = true
  manage_default_vpc                  = true

  # DHCP Options
  enable_dhcp_options                 = true
  dhcp_options_domain_name            = var.domain_name
  dhcp_options_domain_name_servers    = ["AmazonProvidedDNS"]

  # DEFAULT Security Group
  manage_default_security_group       = true
  default_security_group_name         = "${var.instance_tag_name}_SG"
  default_security_group_ingress      = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "in ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]
  default_security_group_egress       = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "out ports"
      cidr_blocks = var.all_range_cidr[0]
    }
  ]

  # DEFAULT Network ACL
  manage_default_network_acl          = true
  default_network_acl_name            = "${var.instance_tag_name}_ACL"
  default_network_acl_ingress         = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "inbound ACL"
      cidr_block  = var.all_range_cidr[0]
      action      = "allow"
      rule_no     = 100
    }
  ]
  public_outbound_acl_rules           = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "outbound ACL"
      cidr_block  = var.all_range_cidr[0]
      action      = "allow"
      rule_no     = 100
    }
  ]

  propagate_public_route_tables_vgw   = true
  propagate_private_route_tables_vgw  = true

  # TAGS
  tags                        = {
    TagGroup                  = "CIAGroup"
  }
  default_security_group_tags = {
    TagGroup                  = "CIAGroup"
  }
  default_network_acl_tags    = {
    TagGroup                  = "CIAGroup"
  }
  dhcp_options_tags           = {
    Name                      = "${var.instance_tag_name}_DHCP"
    TagGroup                  = "CIAGroup"
  }
  nat_gateway_tags            = {
    Name                      = "${var.instance_tag_name}_NatGW"
    TagGroup                  = "CIAGroup"
  }
  nat_eip_tags                = {
    Name                      = "${var.instance_tag_name}_NatEIP"
    TagGroup                  = "CIAGroup"
  }
  igw_tags                    = {
    Name                      = "${var.instance_tag_name}_IGW"
    TagGroup                  = "CIAGroup"
  }
  public_subnet_tags          = {
    Name                      = "${var.instance_tag_name}_Subnet_Public"
    TagGroup                  = "CIAGroup"
  }
  private_subnet_tags         = {
    Name                      = "${var.instance_tag_name}_Subnet_Private"
    TagGroup                  = "CIAGroup"
  }
  public_route_table_tags     = {
    Name                      = "${var.instance_tag_name}_RT_Public"
    TagGroup                  = "CIAGroup"
  }
  private_route_table_tags    = {
    Name                      = "${var.instance_tag_name}_RT_Private"
    TagGroup                  = "CIAGroup"
  }
}